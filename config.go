package main

import (
	"fmt"
	"os"

	"golang.org/x/exp/slices"
	"gopkg.in/yaml.v3"
)

type Config struct {
	Profiles []Profile
}

func validateConfig(config *Config) error {
	if config.Profiles == nil || len(config.Profiles) == 0 {
		return fmt.Errorf("no profiles found")
	}

	names := []string{}
	foundDefault := false
	for index, profile := range config.Profiles {
		profile := profile

		// check for duplicate names
		if slices.Contains(names, profile.Name) {
			return fmt.Errorf("duplicate profile name '%v'", profile.Name)
		}
		names = append(names, profile.Name)

		// check for multiple default profiles
		if profile.IsDefault {
			if foundDefault {
				return fmt.Errorf("multiple default profiles found")
			}
			foundDefault = true
		}

		// validate and possibly fix profile
		err := validateProfile(&profile)
		if err != nil {
			return fmt.Errorf("error validating profile %s: %s", profile.Name, err)
		}
		config.Profiles[index] = profile
	}
	return nil
}

func loadConfig(path string) (Config, error) {
	file, err := os.ReadFile(path)
	if err != nil {
		return Config{}, err
	}

	var config Config
	err = yaml.Unmarshal(file, &config)
	if err != nil {
		return Config{}, err
	}

	err = validateConfig(&config)
	if err != nil {
		return Config{}, err
	}

	return config, nil
}

func saveConfig(path string, config Config) error {
	err := validateConfig(&config)
	if err != nil {
		return err
	}
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()
	encoder := yaml.NewEncoder(file)
	err = encoder.Encode(config)
	if err != nil {
		return err
	}

	return nil
}

func mergeConfigs(a Config, b Config) (Config, error) {
	err := validateConfig(&a)
	if err != nil {
		return Config{}, fmt.Errorf("error validating config a: %s", err)
	}
	err = validateConfig(&b)
	if err != nil {
		return Config{}, fmt.Errorf("error validating config b: %s", err)
	}

	merged := Config{
		Profiles: a.Profiles,
	}
	for _, profile := range b.Profiles {
		index, err := findProfileIndex(merged.Profiles, profile.Name)
		if err == nil {
			merged.Profiles[index] = profile
		} else {
			merged.Profiles = append(merged.Profiles, profile)
		}
	}
	return merged, nil
}

func findDefaultProfile(profiles []Profile) (Profile, error) {
	for _, p := range profiles {
		if p.IsDefault {
			return p, nil
		}
	}
	return Profile{}, fmt.Errorf("unable to find default profile")
}

func findProfileIndex(profiles []Profile, profileName string) (int, error) {
	for index, p := range profiles {
		if p.Name == profileName {
			return index, nil
		}
	}
	return 0, fmt.Errorf("unable to find profile '%v'", profileName)
}
