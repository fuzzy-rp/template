package main

import "time"

func testNow() time.Time {
	return time.Date(2022, 04, 20, 16, 20, 69, 0, time.UTC)
}

func testPrePatternData() PrePatternData {
	return PrePatternData{
		ProjectName: "test-project",
		Year:        "2022",
		Month:       "04",
		Day:         "20",
		Hour:        "16",
		Minute:      "20",
		Second:      "69",
		Extras: map[string]string{
			"key1": "value1",
		},
	}
}
