package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDefaultConfig(t *testing.T) {

	config := defaultConfig()
	err := validateConfig(&config)
	assert.Nil(t, err, "Error validating default config")
}
