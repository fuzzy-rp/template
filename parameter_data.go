package main

import (
	"path/filepath"
	"time"
)

type PrePatternData struct {
	ProjectName                            string
	Year, Month, Day, Hour, Minute, Second string
	YYYY_MM_DD 						   string
	YYYY_MM_DD_HH_MM_SS 				   string
	Extras                                 map[string]string
}

type PostPatternData struct {
	PrePatternData
	FileName                 string
	FileNameWithoutExtension string
	FullPath                 string
}

func createPrePatternData(now time.Time, projectName string, extras map[string]string) PrePatternData {
	prePatternData := PrePatternData{
		Year:        now.Format("2006"),
		Month:       now.Format("01"),
		Day:         now.Format("02"),
		Hour:        now.Format("15"),
		Minute:      now.Format("04"),
		Second:      now.Format("05"),
		YYYY_MM_DD:  now.Format("2006-01-02"),
		YYYY_MM_DD_HH_MM_SS: now.Format("2006-01-02-15-04-05"),
		ProjectName: projectName,
		Extras:      extras,
	}
	return prePatternData
}

func createPostPatternData(fullPath string, prePatternData PrePatternData) PostPatternData {
	fileName := filepath.Base(fullPath)
	fileNameWithoutExt := fileName[:len(fileName)-len(filepath.Ext(fileName))]
	return PostPatternData{
		FullPath:                 fullPath,
		FileName:                 fileName,
		FileNameWithoutExtension: fileNameWithoutExt,
		PrePatternData:           prePatternData,
	}
}
