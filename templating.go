package main

import (
	"fmt"
	"strings"
	"text/template"

	"github.com/Masterminds/sprig"
)

func newTemplate() *template.Template {
	t := template.New("text-template")
	t = t.Option("missingkey=error")
	t = t.Funcs(sprig.FuncMap())
	return t
}

func parseAndExecTemplate(t *template.Template, text ParameterizedText, data any) (string, error) {
	// parse template
	template, err := t.Parse(string(text))
	if err != nil {
		return "", fmt.Errorf("error parsing template text: %v", err)
	}

	// create output string
	sb := new(strings.Builder)
	err = template.Execute(sb, data)
	if err != nil {
		return "", fmt.Errorf("error creating templated file: %v", err)
	}

	templateString := sb.String()
	return templateString, nil
}
