package main

import (
	"bufio"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateFile(t *testing.T) {
	// settings
	path := "~/test/out/test.txt"
	prePatternData := testPrePatternData()
	expectedText := "test-project-2022-04-20-16-20-69-value1"
	var createdFileName = ""

	// monkey patch
	applyTestMonkeyPatch(t, monkeyPatch{
		OSCreate: func(filename string) (*os.File, error) {
			assert.Equal(t, path, filename)
			file, err := os.CreateTemp("./test", "test")
			assert.Nil(t, err, "error creating temp file")
			assert.True(t, createdFileName == "", "createFileName should be empty")
			createdFileName = file.Name()
			return file, nil
		},
		OSWriteFile: func(filename string, data []byte, perm os.FileMode) error {
			assert.Equal(t, path, filename)
			assert.Equal(t, perm, os.ModePerm)
			assert.Equal(t, string(data), expectedText)
			return nil
		},
	})

	// test error path
	err := createFile(prePatternData, "{{.ProjectName", path, nil)
	assert.NotNil(t, err)

	// test happy path with file template
	err = createFile(prePatternData, "{{.ProjectName}}-{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}-{{.Extras.key1}}", path, nil)
	assert.Nil(t, err)

	// test happy path with pipe input
	inputText := `line1 
line2
line3
`
	expectedText = inputText
	reader := strings.NewReader(inputText)
	pipeInputScanner := bufio.NewScanner(reader)
	err = createFile(prePatternData, "{{.ProjectName}}-{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}-{{.Extras.key1}}", path, pipeInputScanner)
	assert.Nil(t, err)
	assert.NotNil(t, createdFileName)
	bytes, err := os.ReadFile(createdFileName)
	assert.Nil(t, err, "error reading file "+createdFileName)
	outputText := string(bytes)
	assert.Equal(t, inputText, outputText)

	// test empty template
	expectedText = ""
	err = createFile(prePatternData, "", path, nil)
	assert.Nil(t, err)
}
