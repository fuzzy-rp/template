package main

func defaultConfig() Config {
	config := Config{
		Profiles: []Profile{
			{
				Name:            "daily",
				Description:     "Creates a MD file in ~/DailyNotes folder in the format YYYY-MM-DD[-vX].md",
				TargetDirectory: "~/DailyNotes",
				Type:            FileType,
				NamePatterns: []ParameterizedText{
					"{{$version := getVersion}}" +
						"{{.YYYY_MM_DD}}" +
						`{{if .ProjectName}}-{{.ProjectName | snakecase }}{{end}}` +
						"{{if (gt $version 1)}}v{{$version}}{{end}}" +
						".md",
				},
				FileTemplate: `# {{.FileNameWithoutExtension}}

Today's notes for {{.Year}}-{{.Month}}-{{.Day}}.

## Todo

1. Check email
2. Check chat
3. Check calendar
4. Check yesterday's notes
5. Have fun!
6. ADD MORE ITEMS HERE`,
				PostScripts: []ParameterizedText{
					"code . {{ if .FullPath }}\"{{.FullPath}}\"{{else}}(ls -t | head -n1){{ end }}",
				},
				RunPostscriptOnFailure: true,
			},
			{
				Name:            "note",
				Description:     "Creates a MD file in ~/Notes in the format YYYY-MM-DD[-ProjectName][-vVersion].md",
				OS:              "All",
				TargetDirectory: "~/Notes",
				Type:            FileType,
				NamePatterns: []ParameterizedText{
					"{{$version := getVersion}}" +
						"{{.Year}}-{{.Month}}-{{.Day}}" +
						"{{if .ProjectName}}-{{.ProjectName}}{{end}}" +
						"{{if (gt $version 1)}}n{{$version}}{{end}}" +
						".md",
				},
				PostScripts: []ParameterizedText{
					"code \"{{.FullPath}}\"",
				},
			},
			{
				Name:            "note-here",
				Description:     "Creates a MD file in the current folder in the format YYYY-MM-DD[-HH[-MM[-SS]]][-ProjectName][-vVersion].md",
				OS:              "All",
				Type:            FileType,
				IsDefault:       true,
				TargetDirectory: ".", // current folder
				NamePatterns: []ParameterizedText{
					"{{$version := getVersion}}" +
						"{{.Year}}-{{.Month}}-{{.Day}}" +
						"{{if .ProjectName}}-{{.ProjectName}}{{end}}" +
						"{{if (gt $version 1)}}n{{$version}}{{end}}" +
						".md",
					"{{$version := getVersion}}" +
						"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}" +
						"{{if .ProjectName}}-{{.ProjectName}}{{end}}" +
						"{{if (gt $version 1)}}n{{$version}}{{end}}" +
						".md",
					"{{$version := getVersion}}" +
						"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}" +
						"{{if .ProjectName}}-{{.ProjectName}}{{end}}" +
						"{{if (gt $version 1)}}n{{$version}}{{end}}" +
						".md",
					"{{$version := getVersion}}" +
						"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}" +
						"{{if .ProjectName}}-{{.ProjectName}}{{end}}" +
						"{{if (gt $version 1)}}n{{$version}}{{end}}" +
						".md",
				},
				PostScripts: []ParameterizedText{
					"code \"{{.FullPath}}\"",
				},
			},
			{
				Name:            "blog",
				Description:     "Creates a MD file in the ~/Blog folder in the format YYYY-MM-DD[-ProjectName][-vVersion].md",
				OS:              "All",
				TargetDirectory: "~/Blog",
				FileTemplate: `# {{.ProjectName}}

Created by template, on {{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}.

## Spelling

cspell:words nanite

## Removed writing

## Writing
`,
				Type: FileType,
				NamePatterns: []ParameterizedText{
					"{{$version := getVersion}}" +
						"{{.Year}}-{{.Month}}-{{.Day}}" +
						`{{if .ProjectName}}-{{.ProjectName | snakecase }}{{end}}` +
						"{{if (gt $version 1)}}v{{$version}}{{end}}" +
						".md",
				},
				PostScripts: []ParameterizedText{
					"code . \"{{.FileName}}\"",
				},
			},
			{
				Name:            "work-win",
				Description:     "Creates a folder in the c:/work folder in the format YYYY-MM-DD[-hh][-mm][-ss][-ProjectName]",
				OS:              "Windows",
				TargetDirectory: "C:/work",
				Type:            FolderType,
				NamePatterns: []ParameterizedText{
					"{{.Year}}-{{.Month}}-{{.Day}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
				},
				PostScripts: []ParameterizedText{
					"explorer \"{{.Path}}\"",
				},
			},
			{
				Name:            "therapy",
				Description:     "Creates a MD file in the ~/Therapy folder in the format YYYY-MM-DD[-vVersion].md",
				OS:              "All",
				TargetDirectory: "~/Therapy",
				FileTemplate: `# {{if .ProjectName}}{{.ProjectName}}{{else}}Therapy notes{{end}}

{{if .ProjectName}}Therapy notes on {{.ProjectName}}{{else}}Therapy notes{{end}}

## Spelling

cspell:words cromulent embiggen

## Writing

{{if .ProjectName}}Hello {{.ProjectName}},{{else}}Hello,{{end}}

Start writing here...

Sincerely, me
`,
				Type: FileType,
				NamePatterns: []ParameterizedText{
					"{{$version := getVersion}}" +
						"{{.Year}}-{{.Month}}-{{.Day}}" +
						"{{if (gt $version 1)}}v{{$version}}{{end}}" +
						".md",
				},
				PostScripts: []ParameterizedText{
					"code . {{.FullPath}}",
				},
			},
			{
				Name:            "work",
				Description:     "Creates a folder in the ~/Work folder in the format YYYY-MM-DD[-hh][-mm][-ss][-ProjectName]",
				TargetDirectory: "~/Work",
				Type:            FolderType,
				NamePatterns: []ParameterizedText{
					"{{.Year}}-{{.Month}}-{{.Day}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
				},
				PostScripts: []ParameterizedText{
					"code .",
					"chmod +x repro.sh",
				},
				Files: []FileInFolder{
					{
						FileTemplate: `# {{.ProjectName}}

Work notes on {{.ProjectName}}
`,
						Name: "README.md",
					},
					{
						FileTemplate: `clear

echo "Let's test {{.ProjectName}}!"`,
						Name: "repro.sh",
					},
					{
						FileTemplate: `{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "bashdb",
            "request": "launch",
            "name": "Debug repo.sh",
            "cwd": "${workspaceFolder}",
            "program": "${workspaceFolder}/repro.sh",
            "terminalKind": "integrated"
        }
    ]
}
							`,
						Name: ".vscode/launch.json",
					},
				},
			},
			{
				Name:            "folder",
				Description:     "Creates a folder in the current folder in the format YYYY-MM-DD[-ProjectName][-vVersion]",
				Type:            FolderType,
				TargetDirectory: ".", // current folder
				PostScripts:     []ParameterizedText{},
				NamePatterns: []ParameterizedText{
					"{{$version := getVersion}}" +
						"{{.Year}}-{{.Month}}-{{.Day}}" +
						"{{if .ProjectName}}-{{.ProjectName}}{{end}}" +
						"{{if (gt $version 1)}}-n{{$version}}{{end}}",
				},
			},
			{
				Name:            "create-react-app",
				Type:            FolderType,
				Description:     "Creates a folder in the current folder in the format create-react-app[-ProjectName][-vVersion], runs `npx create-react-app` to initialize the folder, and opens the folder in VSCode",
				TargetDirectory: ".", // current folder
				NamePatterns: []ParameterizedText{
					"{{$version := getVersion}}" +
						"create-react-app" +
						"{{if .ProjectName}}-{{.ProjectName}}{{end}}" +
						"{{if (gt $version 1)}}-n{{$version}}{{end}}",
				},
				PostScripts: []ParameterizedText{
					"npx create-react-app .",
					"code .",
				},
			},
			{
				Name:            "git",
				Description:     "Creates a  folder in the ~/code folder in the format YYYY-MM-DD[-hh][-mm][-ss][-ProjectName]",
				TargetDirectory: "~/code",
				Type:            FolderType,
				NamePatterns: []ParameterizedText{
					"{{.Year}}-{{.Month}}-{{.Day}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
				},
				PostScripts: []ParameterizedText{
					"git init",
					"code .",
				},
			},
			{
				Name:            "git-clone",
				Description:     "DISABLED - Clones a repository into a new folder under ~/code in the format YYYY-MM-DD[-hh][-mm][-ss][-ProjectName]",
				IsDisabled:      true,
				TargetDirectory: "~/code",
				Type:            FolderType,
				NamePatterns: []ParameterizedText{
					"{{.Year}}-{{.Month}}-{{.Day}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}{{if .ProjectName}}-{{.ProjectName}}{{end}}",
				},
				PostScripts: []ParameterizedText{
					"git clone {{.Url}} .",
					"code .",
				},
			},
			{
				Name:        "log",
				Description: "Creates a log file in the current folder",
				Type:        FileType,
				NamePatterns: []ParameterizedText{
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}{{if .ProjectName}}-{{.ProjectName}}{{end}}.log",
				},
				TargetDirectory: ".",
				PostScripts: []ParameterizedText{
					"code . {{.FullPath}}",
				},
			},

			{
				Name:        "dolt",
				Description: "Creates a `YYYY-MM-DD-ProjectName` folder for testing SQL in dolt and MySQL",
				Type:        FolderType,
				NamePatterns: []ParameterizedText{
					"{{.Year}}-{{.Month}}-{{.Day}}-{{.ProjectName}}",
				},
				TargetDirectory: "~/Work",
				PostScripts: []ParameterizedText{
					"code .",
					"chmod +x repro.sh",
				},
				Files: []FileInFolder{
					{
						Name: "README.md",
						FileTemplate: `# {{.ProjectName}}

Work notes on {{.ProjectName}}`,
					},
					{
						Name: ".vscode/launch.json",
						FileTemplate: `{
	// Use IntelliSense to learn about possible attributes.
	// Hover to view descriptions of existing attributes.
	// For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
	"version": "0.2.0",
	"configurations": [
		{
			"type": "bashdb",
			"request": "launch",
			"name": "Debug repo.sh",
			"cwd": "${workspaceFolder}",
			"program": "${workspaceFolder}/repro.sh",
			"terminalKind": "integrated"
		}
	]
}`,
					},
					{
						Name: "repro.sh",
						FileTemplate: `clear

echo "Let's test {{.ProjectName}}!"

echo "setting up dolt"
rm -rf db
mkdir db
cd db
dolt init
dolt version

echo "running against dolt $(dolt version)"

echo "dolt setup.sql"
echo ">>>>>>>>>>>>>>>>>>>>>>"
dolt sql < ../setup.sql
echo "<<<<<<<<<<<<<<<<<<<<<<"

echo "dolt test.sql"
echo ">>>>>>>>>>>>>>>>>>>>>>"
dolt sql < ../test.sql
echo "<<<<<<<<<<<<<<<<<<<<<<"

echo "mysql start"
mysql.server start
mysql -u root -e "drop database if exists db; create database db;"

echo "mysql setup.sql"
echo ">>>>>>>>>>>>>>>>>>>>>>"
cat ../setup.sql | mysql -u root --table
echo "<<<<<<<<<<<<<<<<<<<<<<"

echo "mysql test.sql"
echo ">>>>>>>>>>>>>>>>>>>>>>"
cat ../test.sql | mysql -u root --table
echo "<<<<<<<<<<<<<<<<<<<<<<"

echo "stop mysql"
killall mysqld mysqld_safe`,
					},
					{
						Name: "setup.sql",
						FileTemplate: `use db;

SET foreign_key_checks=0;

CREATE TABLE t(a INT, b INT);
INSERT INTO t(a, b) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(2, 6);

SET foreign_key_checks=1;`,
					},
					{
						Name: "test.sql",
						FileTemplate: `use db;

SELECT SUM(b) OVER (PARTITION BY a) FROM t ORDER BY 1;
SELECT SUM(b) OVER () FROM t ORDER BY 1;
SELECT SUM(b) OVER (PARTITION BY a), SUM(b) OVER () FROM t;`,
					},
				},
			},
		},
	}

	return config
}
