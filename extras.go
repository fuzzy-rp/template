package main

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

func loadExtras(path string) (map[string]string, error) {
	emptyMap := make(map[string]string)

	file, err := os.ReadFile(path)
	if err != nil {
		return emptyMap, fmt.Errorf("error reading extras file %v: %v", path, err)
	}

	result := make(map[string]string)
	err = yaml.Unmarshal(file, &result)
	if err != nil {
		return emptyMap, err
	}

	return result, nil
}
