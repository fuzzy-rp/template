package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"golang.org/x/exp/slices"
)

// Mon Jan 2 15:04:05 MST 2006
var now = time.Date(2006, 1, 2, 15, 4, 5, 0, time.UTC)

func TestCreateNewName(t *testing.T) {
	// setup code

	extras := make(map[string]string)
	extras["test1"] = "test1"

	profile := Profile{
		IsDefault: true,
		Type:      FileType,
		NamePatterns: []ParameterizedText{
			"{{.Extras.test1}}-{{.Year}}-{{.Month}}-{{.Day}}{{if .ProjectName}}-{{.ProjectName}}{{end}}.txt",
			"{{.Extras.test1}}-{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}{{if .ProjectName}}-{{.ProjectName}}{{end}}.txt",
			"{{.Extras.test1}}-{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}{{if .ProjectName}}-{{.ProjectName}}{{end}}.txt",
		},
	}
	newNameNoProject := "test1-2006-01-02.txt"
	newName1 := "test1-2006-01-02-test-project.txt"
	newName2 := "test1-2006-01-02-15-test-project.txt"
	newName3 := "test1-2006-01-02-15-04-05-test-project.txt"

	testHappyPath := func(projectName string, expectedName string, existingEntities []string) func(t *testing.T) {
		return func(t *testing.T) {
			prePatternData := createPrePatternData(now, projectName, extras)
			validator := func(newName string) bool { return !slices.Contains(existingEntities, newName) }
			newName, err := createNewNameForPatterns(now, prePatternData, projectName, profile.NamePatterns, extras, validator)
			assert.NoError(t, err)
			assert.Equal(t, expectedName, newName, "generated name")
			assert.Equal(t, projectName, prePatternData.ProjectName, "project name")
			assert.Equal(t, "2006", prePatternData.Year, "year")
			assert.Equal(t, "01", prePatternData.Month, "month")
			assert.Equal(t, "02", prePatternData.Day, "day")
			assert.Equal(t, "15", prePatternData.Hour, "hour")
			assert.Equal(t, "04", prePatternData.Minute, "minute")
			assert.Equal(t, "05", prePatternData.Second, "second")
			assert.Equal(t, "2006-01-02", prePatternData.YYYY_MM_DD, "YYYY_MM_DD")
			assert.Equal(t, "2006-01-02-15-04-05", prePatternData.YYYY_MM_DD_HH_MM_SS, "YYYY_MM_DD_HH_MM_SS")
			assert.Equal(t, "test1", prePatternData.Extras["test1"], "test1")
		}
	}
	testErrorPath := func(projectName string, existingEntities []string) func(t *testing.T) {
		return func(t *testing.T) {
			prePatternData := createPrePatternData(now, projectName, extras)
			validator := func(newName string) bool { return !slices.Contains(existingEntities, newName) }
			_, err := createNewNameForPatterns(now, prePatternData, projectName, profile.NamePatterns, extras, validator)
			if err == nil {
				t.Error("Expected error but got none")
			}
		}
	}

	// test code

	// happy path
	t.Run("createNewName - happy path", testHappyPath("test-project", newName1, []string{}))
	t.Run("createNewName - use second pattern", testHappyPath("test-project", newName2, []string{newName1}))
	t.Run("createNewName - use third pattern", testHappyPath("test-project", newName3, []string{newName1, newName2}))
	t.Run("createNewName - happy path", testHappyPath("", newNameNoProject, []string{}))

	// error path
	t.Run("createNewName - error path - collision", testErrorPath("test-project", []string{newName1, newName2, newName3}))
}

func TestCreateNewName_Index(t *testing.T) {
	extras := make(map[string]string)
	extras["test1"] = "test1"

	profile := Profile{
		IsDefault: true,
		Type:      FileType,
		NamePatterns: []ParameterizedText{
			"{{.Extras.test1}}-{{$version := getVersion}}{{.Year}}-{{.Month}}-{{.Day}}v{{$version}}.txt",
		},
	}
	newName1 := "test1-2006-01-02v1.txt"
	newName2 := "test1-2006-01-02v2.txt"
	newName3 := "test1-2006-01-02v3.txt"

	testHappyPath := func(expectedName string, existingEntities []string) func(t *testing.T) {
		return func(t *testing.T) {
			prePatternData := createPrePatternData(now, "test-project", extras)
			validator := func(newName string) bool { return !slices.Contains(existingEntities, newName) }
			newName, err := createNewNameForPatterns(now, prePatternData, "test-project", profile.NamePatterns, extras, validator)
			assert.NoError(t, err)
			assert.Equal(t, expectedName, newName, "generated name")
			assert.Equal(t, "test-project", prePatternData.ProjectName, "project name")
			assert.Equal(t, "2006", prePatternData.Year, "year")
			assert.Equal(t, "01", prePatternData.Month, "month")
			assert.Equal(t, "02", prePatternData.Day, "day")
			assert.Equal(t, "15", prePatternData.Hour, "hour")
			assert.Equal(t, "04", prePatternData.Minute, "minute")
			assert.Equal(t, "05", prePatternData.Second, "second")
			assert.Equal(t, "2006-01-02", prePatternData.YYYY_MM_DD, "YYYY_MM_DD")
			assert.Equal(t, "2006-01-02-15-04-05", prePatternData.YYYY_MM_DD_HH_MM_SS, "YYYY_MM_DD_HH_MM_SS")
		}
	}

	// test code

	// happy path
	t.Run("createNewName - v1", testHappyPath(newName1, []string{}))
	t.Run("createNewName - v2", testHappyPath(newName2, []string{newName1}))
	t.Run("createNewName - v3", testHappyPath(newName3, []string{newName1, newName2}))
	t.Run("createNewName - v2", testHappyPath(newName2, []string{newName1, newName3}))
}

func TestCreateNewName_sprig(t *testing.T) {
	profile := Profile{
		IsDefault:    true,
		Type:         FileType,
		NamePatterns: []ParameterizedText{`{{"HELLO" | lower}}`},
	}
	projectName := "test-project"
	prePatternData := createPrePatternData(now, projectName, nil)
	newName, err := createNewNameForPatterns(
		now,
		prePatternData,
		projectName,
		profile.NamePatterns,
		nil,
		func(newName string) bool {
			return true
		})
	assert.Nil(t, err)
	assert.Equal(t, "hello", newName)
}
