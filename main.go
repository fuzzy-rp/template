package main

import (
	"bufio"
	"fmt"
	"io/fs"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2" // imports as package "cli"
	"gopkg.in/yaml.v3"
)

var (
	// Version is the version of the application
	Version string
)

const defaultUserConfigPath = "~/.template.user.yaml"
const defaultConfigPath = "~/.template.yaml"
const defaultLogLevel = "info"

type ArgumentValues struct {
	profile        string
	projectName    string
	fileTemplate   string
	dirTemplate    string
	scriptTemplate string
	configFilePath string
	userConfigPath string
	extrasFilePath string
	logLevel       string
}
type EnvVarValues struct {
	useDefaultConfig bool
}
type Settings struct {
	ArgumentValues
	EnvVarValues
	extras         map[string]string
	fullConfigPath string
	config         Config
	usr            *user.User
}

func main() {
	argumentValues := ArgumentValues{}

	app := &cli.App{
		Name:                 "template",
		Usage:                "create file or directory from a template",
		EnableBashCompletion: true,
		Suggest:              true,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "profile",
				Aliases:     []string{"p"},
				Value:       "",
				Usage:       "`PROFILE` to use for the template",
				Destination: &argumentValues.profile,
				// Value:       defaultProfileName,
				// Required:    true,
				// EnvVars: []string{"TEMPLATE_PROFILE"},
			},
			&cli.StringFlag{
				Name:        "file",
				Aliases:     []string{"f"},
				Value:       "",
				Usage:       "file template to use to create a file",
				Destination: &argumentValues.fileTemplate,
			},
			&cli.StringFlag{
				Name:        "dir",
				Aliases:     []string{"d"},
				Value:       "",
				Usage:       "folder pattern to use to create a directory",
				Destination: &argumentValues.dirTemplate,
			},
			&cli.StringFlag{
				Name:        "script",
				Aliases:     []string{"s"},
				Value:       "",
				Usage:       "templated script to run in the shell",
				Destination: &argumentValues.scriptTemplate,
			},
			&cli.StringFlag{
				Name:        "name",
				Aliases:     []string{"n"},
				Usage:       "name of the project to create",
				Value:       "",
				Destination: &argumentValues.projectName,
			},
			&cli.PathFlag{
				Name:        "config-yaml",
				Aliases:     []string{"c"},
				Usage:       "path to the config yaml file",
				Value:       defaultConfigPath,
				Destination: &argumentValues.configFilePath,
			},
			&cli.PathFlag{
				Name:        "user-config-yaml",
				Aliases:     []string{"u"},
				Usage:       "path to the user config yaml file",
				Value:       defaultUserConfigPath,
				Destination: &argumentValues.userConfigPath,
			},
			&cli.PathFlag{
				Name:        "extras-yaml",
				Aliases:     []string{"e"},
				Usage:       "path to the extras yaml file",
				Value:       "~/.template-extras.yaml",
				Destination: &argumentValues.extrasFilePath,
			},
			&cli.StringFlag{
				Name:        "log-level",
				Aliases:     []string{"l"},
				Usage:       "log level",
				Value:       defaultLogLevel,
				Destination: &argumentValues.logLevel,
			},
		},
		Commands: []*cli.Command{
			{
				Name:  "list",
				Usage: "list available profiles",
				Action: func(c *cli.Context) error {
					settings, err := createSettings(argumentValues, c)
					if err != nil {
						return fmt.Errorf("error creating settings: %v", err)
					}

					config := settings.config
					// list profiles
					fmt.Println("Available profiles:")
					for _, p := range config.Profiles {
						fmt.Printf("  %v: %v\n", p.Name, p.Description)
					}
					fmt.Println()
					return nil
				},
			},
			{
				Name:  "create",
				Usage: "create a fully-hardcoded template profile from a file or folder",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "dir",
						Aliases:     []string{"d"},
						Value:       "",
						Usage:       "folder from which to create the template",
						Destination: &argumentValues.dirTemplate,
					},
					&cli.StringFlag{
						Name:        "file",
						Aliases:     []string{"f"},
						Value:       "",
						Usage:       "file from which to create the template",
						Destination: &argumentValues.fileTemplate,
					},
				},
				Action: func(c *cli.Context) error {
					// get args
					c.Args()

					filePath := argumentValues.fileTemplate
					dirPath := argumentValues.dirTemplate
					if filePath == "" && dirPath == "" {
						return fmt.Errorf("file or dir not specified, use `file` or `dir` flag")
					}

					profileName := argumentValues.profile
					if profileName == "" {
						profileName = "temp"
					}

					sourcePath := filePath
					profileType := FileType
					if dirPath != "" {
						profileType = FolderType
						sourcePath = dirPath
					}
					profile := Profile{
						Name:            profileName,
						Description:     fmt.Sprintf("temp %s profile generated from %s", profileType, sourcePath),
						Type:            profileType,
						TargetDirectory: ".",
					}

					dir, file := path.Split(sourcePath)
					profile.TargetDirectory = dir
					profile.NamePatterns = []ParameterizedText{
						ParameterizedText(file),
					}
					profile.PostScripts = []ParameterizedText{
						ParameterizedText(fmt.Sprintf("code ./%s", file)),
					}

					if filePath != "" {
						file, err := readFile(filePath)
						if err != nil {
							return fmt.Errorf("error reading file %s: %w", filePath, err)
						}
						template := ParameterizedText(file)
						profile.FileTemplate = template
					} else {
						files := []FileInFolder{}
						err := filepath.WalkDir(dirPath, func(path string, d fs.DirEntry, err error) error {
							if err != nil {
								fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
								return err
							}

							isRoot := path == dirPath
							isDir := d.IsDir()
							shouldSkip := isRoot || isDir
							if shouldSkip {
								return nil
							}
							relPath, err := filepath.Rel(dirPath, path)
							if err != nil {
								return fmt.Errorf("error getting relative path for %s: %w", path, err)
							}
							contents, err := readFile(path)
							if err != nil {
								return fmt.Errorf("error reading file %s: %w", path, err)
							}
							fif := FileInFolder{
								Name:         ParameterizedText(relPath),
								FileTemplate: ParameterizedText(contents),
							}
							files = append(files, fif)
							return nil
						})
						if err != nil {
							return fmt.Errorf("error walking dir %s: %w", dirPath, err)
						}
						profile.Files = files
					}

					config := Config{
						Profiles: []Profile{profile},
					}

					writer := strings.Builder{}
					encoder := yaml.NewEncoder(&writer)
					err := encoder.Encode(config)
					if err != nil {
						return fmt.Errorf("error encoding config: %v", err)
					}

					yamlText := writer.String()
					fmt.Println(yamlText)
					return nil
				},
			},
			{
				Name:  "get",
				Usage: "get a profile",
				Action: func(c *cli.Context) error {
					settings, err := createSettings(argumentValues, c)
					if err != nil {
						return fmt.Errorf("error creating settings: %v", err)
					}
					config := settings.config

					// get profile information
					index, err := findProfileIndex(config.Profiles, settings.profile)
					if err != nil {
						return fmt.Errorf("profile '%v' not found", settings.profile)
					}
					profile := config.Profiles[index]

					writer := strings.Builder{}
					encoder := yaml.NewEncoder(&writer)
					err = encoder.Encode(profile)
					if err != nil {
						return fmt.Errorf("error encoding profile: %v", err)
					}

					yamlText := writer.String()
					fmt.Printf("Profile '%v':\n", settings.profile)
					fmt.Println(yamlText)
					fmt.Println()
					return nil
				},
			},
			{
				Name:  "reset",
				Usage: "reset the global template config to default profiles",
				Action: func(c *cli.Context) error {
					usr, err := user.Current()
					if err != nil {
						log.Fatal(err)
					}
					fullConfigPath := getFullPath(argumentValues.configFilePath, *usr)

					fmt.Printf("Resetting config at '%v' to default\n", fullConfigPath)
					err = saveConfig(fullConfigPath, defaultConfig())
					if err != nil {
						return fmt.Errorf("error saving default config: %v", err)
					}
					fmt.Println()
					return nil
				},
			},
			{
				Name:  "version",
				Usage: "print version",
				Action: func(c *cli.Context) error {
					fmt.Print(Version)
					fmt.Println()
					return nil
				},
			},
		},
		Action: func(context *cli.Context) error {
			settings, err := createSettings(argumentValues, context)
			if err != nil {
				return fmt.Errorf("error parsing arguments: %w", err)
			}
			err = mainLogic(settings)
			if err != nil {
				return fmt.Errorf("error in main logic: %w", err)
			}
			fmt.Println()
			return nil
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func createSettings(
	argumentValues ArgumentValues,
	context *cli.Context,
) (Settings, error) {
	envVarValues, err := readEnvVars()
	if err != nil {
		return Settings{}, fmt.Errorf("error getting env var values: %w", err)
	}

	settings := Settings{
		ArgumentValues: argumentValues,
		EnvVarValues:   envVarValues,
	}

	// read args
	args := context.Args()

	// configure logging
	var level log.Level
	switch strings.ToLower(settings.logLevel) {
	case "debug":
		level = log.DebugLevel
	case "info":
		level = log.InfoLevel
	case "warn":
		level = log.WarnLevel
	case "error":
		level = log.ErrorLevel
	case "fatal":
		level = log.FatalLevel
	case "panic":
		level = log.PanicLevel
	default:
		return Settings{}, fmt.Errorf("invalid log level specified %v", settings.logLevel)
	}
	log.SetLevel(level)
	log.SetOutput(os.Stdout)

	log.WithFields(log.Fields{"args": args}).Debug("Args parsed")

	skipProfile := len(settings.fileTemplate) > 0 || len(settings.dirTemplate) > 0 || len(settings.scriptTemplate) > 0
	currentParamIndex := 0

	if !skipProfile {
		// get profile name from args
		// if not set explicitly, use first arg, if present
		if settings.profile == "" && args.Len() > currentParamIndex {
			settings.profile = args.Get(currentParamIndex)
		}
		fmt.Println("Profile:", settings.profile)
		currentParamIndex++
	}

	// get project name from args
	// if not set explicitly, use next arg (after profile), if present
	if settings.projectName == "" && args.Len() > currentParamIndex {
		settings.projectName = args.Get(currentParamIndex)
		// currentParamIndex++
	}
	fmt.Println("Project name:", settings.projectName)

	// extras strings
	_, err = os.Stat(settings.extrasFilePath)
	if err == nil {
		extras, err := loadExtras(settings.extrasFilePath)
		if err != nil {
			return settings, fmt.Errorf("error loading extras from %v: %w", settings.extrasFilePath, err)
		}
		settings.extras = extras
	}

	// get user
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	settings.usr = usr

	// get config from default path
	fullConfigPath := getFullPath(settings.configFilePath, *usr)
	settings.fullConfigPath = fullConfigPath

	var fullConfig Config
	if settings.useDefaultConfig {
		fullConfig = defaultConfig()
	} else {
		config, err := loadConfig(fullConfigPath)
		if err != nil {
			return settings, fmt.Errorf("error loading config from %v: %w\nConsider running `template reset` to recreate the config file. Keep in mind that `reset` will remove existing config.", fullConfigPath, err)
		}
		fmt.Printf("Loaded config from %v\n", fullConfigPath)

		// load user config, if present
		fullConfig = loadAndMergeUserConfig(settings.userConfigPath, usr, config)
	}
	settings.config = fullConfig

	return settings, nil
}

func loadAndMergeUserConfig(userConfigPath string, usr *user.User, globalConfig Config) Config {
	fullUserConfigPath := getFullPath(userConfigPath, *usr)
	_, err := os.Stat(fullUserConfigPath)
	if err != nil {
		fmt.Printf("User config file not found at %v, skipping\n", fullUserConfigPath)
		return globalConfig
	}

	userConfig, err := loadConfig(fullUserConfigPath)
	if err != nil {
		fmt.Printf("Error loading user config from %v: %v\n", fullUserConfigPath, err)
		return globalConfig
	}

	fmt.Printf("Loaded user config from %v, combining with global config\n", fullUserConfigPath)
	fullConfig, err := mergeConfigs(globalConfig, userConfig)
	if err != nil {
		fmt.Printf("Error merging configs: %v\n", err)
	}

	return fullConfig
}

func readEnvVars() (EnvVarValues, error) {
	envVarValues := EnvVarValues{}

	useDefaultConfigText := os.Getenv("TEMPLATE_USE_DEFAULT_CONFIG")
	if useDefaultConfigText != "" {
		useDefaultConfig, err := strconv.ParseBool(useDefaultConfigText)
		if err != nil {
			return envVarValues, fmt.Errorf("error parsing env var TEMPLATE_USE_DEFAULT_CONFIG: \"%s\" %w", useDefaultConfigText, err)
		}
		envVarValues.useDefaultConfig = useDefaultConfig
	}

	return envVarValues, nil
}

func findProfile(settings Settings) (Profile, error) {
	var profile Profile = Profile{}
	var err error
	config := settings.config
	createTempProfile := len(settings.fileTemplate) > 0 || len(settings.dirTemplate) > 0 || len(settings.scriptTemplate) > 0

	if createTempProfile {
		if len(settings.scriptTemplate) > 0 {
			profile = Profile{
				Name:        "temp",
				Description: "temp-script-profile",
				Type:        ScriptType,
				PostScripts: []ParameterizedText{ParameterizedText(settings.scriptTemplate)},
			}
		} else {
			profileType := FileType
			if len(settings.dirTemplate) > 0 {
				profileType = FolderType
			}
			patterns := []ParameterizedText{}
			if len(settings.fileTemplate) > 0 {
				patterns = append(patterns, ParameterizedText(settings.fileTemplate))
			} else if len(settings.dirTemplate) > 0 {
				patterns = append(patterns, ParameterizedText(settings.dirTemplate))
			}
			profile = Profile{
				Name:         "temp",
				Description:  fmt.Sprintf("temp-%s-profile", profileType),
				Type:         profileType,
				NamePatterns: patterns,
			}
		}
	} else {
		if settings.profile == "" {
			profile, err = findDefaultProfile(config.Profiles)
			if err != nil {
				return profile, fmt.Errorf("no profile specified and no default profile found")
			}
		} else {
			index, err := findProfileIndex(config.Profiles, settings.profile)
			if err != nil {
				return profile, fmt.Errorf("error finding profile: %v", err)
			}
			profile = config.Profiles[index]
		}
	}
	return profile, nil
}

func mainLogic(settings Settings) error {
	// find profile
	profile, err := findProfile(settings)
	if err != nil {
		return fmt.Errorf("error finding profile: %v", err)
	}

	stat, err := os.Stdin.Stat()
	if err != nil {
		return fmt.Errorf("error getting stdin stat: %w", err)
	}
	var pipeInputScanner *bufio.Scanner = nil
	incomingPipeData := (stat.Mode() & os.ModeCharDevice) == 0
	if incomingPipeData {
		pipeInputScanner = bufio.NewScanner(os.Stdin)
		fmt.Printf("Reading from stdin\n")
	}

	// process profile
	errors := processProfile(profile, settings.projectName, settings.extras, *settings.usr, pipeInputScanner)
	if len(errors) > 0 {
		return fmt.Errorf("error processing profile: %v", errors)
	}

	return nil
}
