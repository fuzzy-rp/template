package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	shutdown()
	os.Exit(code)
}

func setup() {
	// setup code
}
func shutdown() {
	// shutdown code

	// not really necessary, but why not?
	undoMonkeyPatch()
}

func TestFindProfile(t *testing.T) {
	fileTemplate := "{{.Year}}-{{.Month}}-{{.Day}}{{if .ProjectName}}-{{.ProjectName}}{{end}}.txt"
	profile, err := findProfile(Settings{
		ArgumentValues: ArgumentValues{
			fileTemplate: fileTemplate,
		},
	})
	assert.NoError(t, err)
	assert.Equal(t, "temp", profile.Name)
	assert.Equal(t, FileType, profile.Type)
	assert.Equal(t, 1, len(profile.NamePatterns))
	assert.Equal(t, ParameterizedText(fileTemplate), profile.NamePatterns[0])

	dirTemplate := "{{.Year}}-{{.Month}}-{{.Day}}{{if .ProjectName}}-{{.ProjectName}}{{end}}"
	profile, err = findProfile(Settings{
		ArgumentValues: ArgumentValues{
			dirTemplate: dirTemplate,
		},
	})
	assert.NoError(t, err)
	assert.Equal(t, "temp", profile.Name)
	assert.Equal(t, FolderType, profile.Type)
	assert.Equal(t, 1, len(profile.NamePatterns))
	assert.Equal(t, ParameterizedText(dirTemplate), profile.NamePatterns[0])
}
