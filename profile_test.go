package main

import (
	"io/fs"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"runtime"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestValidateProfile(t *testing.T) {
	testSuccess := func(profile Profile) {
		err := validateProfile(&profile)
		if err != nil {
			t.Errorf("Expected to validate profile '%v', but failed: %v", profile.Name, err)
		}
	}
	testFailure := func(profile Profile, expectedErrStr string) {
		err := validateProfile(&profile)
		assert.NotNil(t, err, "Expected to fail to validate profile '%v', but succeeded")
		assert.Contains(t, err.Error(), expectedErrStr, "Expected error to contain '%v', but it didn't", expectedErrStr)
	}

	testSuccess(Profile{
		Type:            FileType,
		Name:            "daily",
		NamePatterns:    []ParameterizedText{"{{.Date}}"},
		TargetDirectory: "~/test/dir",
	})
	testSuccess(Profile{
		Type:            FileType,
		Name:            "daily",
		NamePatterns:    []ParameterizedText{"{{.Date}}"},
		FileTemplate:    "foo",
		TargetDirectory: "~/test/dir",
	})

	testFailure(Profile{
		Name: "",
	}, "profile name is required")
	testFailure(Profile{
		Name: "list",
	}, "forbidden profile name 'list'")
	testFailure(Profile{
		Name: "get",
	}, "forbidden profile name 'get'")
	testFailure(Profile{
		Type: FileType,
		Name: "daily",
	}, "namePatterns is required")
	testFailure(Profile{
		Type:         FileType,
		Name:         "daily",
		NamePatterns: []ParameterizedText{"blah"},
	}, "targetDirectory is not specified for profile daily, which has Type=file")

	testFailure(Profile{
		Type:         FolderType,
		Name:         "daily",
		NamePatterns: []ParameterizedText{"test"},
	}, "targetDirectory is not specified for profile daily, which has Type=folder")
	testFailure(Profile{
		Type:         FolderType,
		Name:         "daily",
		NamePatterns: []ParameterizedText{"test"},
		FileTemplate: "bar",
	}, "targetDirectory is not specified for profile daily, which has Type=folder")
	testFailure(Profile{
		Type:         ScriptType,
		Name:         "daily",
		NamePatterns: []ParameterizedText{"test", "test2"},
	}, "multiple name patterns are not supported for profile daily, which has Type=script")
	testFailure(Profile{
		Type:         FileType,
		Name:         "daily",
		NamePatterns: []ParameterizedText{"test", "test2"},
	}, "targetDirectory is not specified for profile daily, which has Type=file")
	testFailure(Profile{
		Type:         FileType,
		Name:         "daily",
		NamePatterns: []ParameterizedText{"test", ""},
	}, "name pattern is required for pattern index 1 of profile daily")

	for _, profile := range defaultConfig().Profiles {
		testSuccess(profile)
	}
}

func TestFindProfileIndex(t *testing.T) {
	config := Config{
		Profiles: []Profile{
			{
				Name: "daily",
			},
			{
				IsDefault: true,
				Name:      "notes",
			},
		},
	}
	testSuccess := func(profileName string, expectedProfileName string) {
		index, err := findProfileIndex(config.Profiles, profileName)
		if err != nil {
			t.Errorf("Failed to find profile '%v': %v", profileName, err)
		}
		profile := config.Profiles[index]
		if profile.Name != expectedProfileName {
			t.Errorf("Asked for profile '%v', expected '%v', got '%v'", profileName, expectedProfileName, profile.Name)
		}
	}
	testFailure := func(profileName string) {
		_, err := findProfileIndex(config.Profiles, profileName)
		if err == nil {
			t.Errorf("Expected to fail to find profile '%v', but succeeded", profileName)
		}
	}

	testSuccess("daily", "daily")
	testSuccess("notes", "notes")

	testFailure("")
	testFailure("non-existent")

	config.Profiles[1].IsDefault = false
	testFailure("")
	testFailure("non-default")
}

func TestFindDefaultProfile(t *testing.T) {
	_, err := findDefaultProfile(defaultConfig().Profiles)
	assert.Nil(t, err, "Expected to find default profile, but failed")

	_, err = findDefaultProfile([]Profile{})
	assert.NotNil(t, err, "Expected to fail to find default profile, but succeeded")
}

func TestCreateEntity(t *testing.T) {
	// setup
	now := testNow()
	cwd := "~/test/dir"
	projectName := "project-name"
	extras := map[string]string{
		"foo": "bar",
	}
	patternData := testPrePatternData()

	testFolder := func() {
		monkeyPatchCalls := applyTestMonkeyPatch(t, monkeyPatch{
			OSMkdir:     func(path string, perm os.FileMode) error { return nil },
			OSChdir:     func(dir string) error { return nil },
			OSWriteFile: func(filename string, data []byte, perm os.FileMode) error { return nil },
		})

		profile := Profile{
			Type: FolderType,
			Files: []FileInFolder{
				{
					Name:         "readme.md",
					FileTemplate: "blah",
				},
				{
					Name:         "test.txt",
					FileTemplate: "foo",
				},
			},
		}

		newName := "new-name"
		expectedFullPath := filepath.Clean("~/test/dir/" + newName)
		fullPath, err := createEntity(now, profile, cwd, newName, projectName, extras, patternData, nil)
		assert.Nil(t, err)
		assert.Equal(t, expectedFullPath, fullPath)
		assert.Equal(t, 1, len(monkeyPatchCalls.OSChdirCalls))
		assert.Equal(t, expectedFullPath, monkeyPatchCalls.OSChdirCalls[0].dir)
		assert.Equal(t, 3, len(monkeyPatchCalls.OSMkdirCalls))
		assert.Equal(t, expectedFullPath, monkeyPatchCalls.OSMkdirCalls[0].path)
		assert.Equal(t, 2, len(monkeyPatchCalls.OSWriteFileCalls))
		assert.Equal(t, filepath.Clean(expectedFullPath+"/readme.md"), monkeyPatchCalls.OSWriteFileCalls[0].filename)
		assert.Equal(t, []byte("blah"), monkeyPatchCalls.OSWriteFileCalls[0].data)
		assert.Equal(t, filepath.Clean(expectedFullPath+"/test.txt"), monkeyPatchCalls.OSWriteFileCalls[1].filename)
		assert.Equal(t, []byte("foo"), monkeyPatchCalls.OSWriteFileCalls[1].data)
	}

	testFile := func() {
		monkeyPatchCalls := applyTestMonkeyPatch(t, monkeyPatch{
			OSWriteFile: func(filename string, data []byte, perm os.FileMode) error { return nil },
		})

		profile := Profile{
			Type:         FileType,
			FileTemplate: "blah",
		}
		newName := "new-name.md"
		expectedFullPath := filepath.Clean("~/test/dir/" + newName)
		fullPath, err := createEntity(now, profile, cwd, newName, projectName, extras, patternData, nil)
		assert.Nil(t, err)
		assert.Equal(t, expectedFullPath, fullPath)
		assert.Equal(t, 1, len(monkeyPatchCalls.OSWriteFileCalls))
		assert.Equal(t, expectedFullPath, monkeyPatchCalls.OSWriteFileCalls[0].filename)
		assert.Equal(t, []byte("blah"), monkeyPatchCalls.OSWriteFileCalls[0].data)
	}

	testScript := func() {
		profile := Profile{
			Type: ScriptType,
			PostScripts: []ParameterizedText{
				"{{.ProjectName}}-{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}-{{.Extras.key1}}",
			},
		}
		newName := "new-name.md"
		expectedFullPath := filepath.Clean("~/test/dir/" + newName)
		fullPath, err := createEntity(now, profile, cwd, newName, projectName, extras, patternData, nil)
		assert.Nil(t, err)
		assert.Equal(t, expectedFullPath, fullPath)
	}

	testFolder()

	testFile()

	testScript()
}

type testFile struct {
	name  string
	isDir bool
}

func (f testFile) Name() string               { return f.name }
func (f testFile) IsDir() bool                { return f.isDir }
func (f testFile) Type() fs.FileMode          { return 0 }
func (f testFile) Info() (fs.FileInfo, error) { return nil, nil }
func (f testFile) ModTime() time.Time         { return time.Time{} }
func (f testFile) Mode() fs.FileMode          { return 0 }
func (f testFile) Size() int64                { return 0 }
func (f testFile) Sys() any                   { return nil }

var _ os.DirEntry = testFile{}
var _ os.FileInfo = testFile{}

func createDirEntry(name string, isDir bool) testFile {
	entry := testFile{name: name, isDir: isDir}
	return entry
}

func TestProcessProfile(t *testing.T) {
	cwd := filepath.Clean("~/test/dir")
	var monkeyPatchCalls *monkeyPatchCalls
	configureMonkeyPatching := func() {
		monkeyPatchCalls = applyTestMonkeyPatch(t, monkeyPatch{
			ExecCommand: func(command string, args ...string) *exec.Cmd {
				shell, shellArgs := shellCommandAndArgs("echo")
				return exec.Command(shell, shellArgs...)
			},
			OSCreate: func(name string) (file *os.File, err error) {
				dir := os.TempDir()
				return os.CreateTemp(dir, "test")
			},
			OSChdir: func(dir string) error { return nil },
			OSGetwd: func() (dir string, err error) { return cwd, nil },
			OSReadDir: func(dirname string) ([]os.DirEntry, error) {
				return []os.DirEntry{
					createDirEntry("dir1", true),
					createDirEntry("dir2", true),
					createDirEntry("file1", false),
					createDirEntry("file2", false),
				}, nil
			},
			OSStat: func(name string) (os.FileInfo, error) {
				return createDirEntry("dir1", true), nil
			},
			OSWriteFile: func(filename string, data []byte, perm os.FileMode) error {
				return nil
			},
		})
	}

	projectName := "project-name"
	extras := map[string]string{
		"foo": "bar",
	}

	usr, err := user.Current()
	assert.Nil(t, err)
	assert.NotNil(t, usr)

	testSuccess := func(profile Profile) {
		configureMonkeyPatching()
		errors := processProfile(profile, projectName, extras, *usr, nil)
		assert.Equal(t, 0, len(errors))
		for _, error := range errors {
			assert.Nil(t, error)
		}
	}
	testFailure := func(profile Profile, errorCount int, errorStr string) {
		configureMonkeyPatching()
		errors := processProfile(profile, projectName, extras, *usr, nil)
		assert.Equal(t, errorCount, len(errors))
		allErrorsText := ""
		for _, err := range errors {
			allErrorsText += err.Error()
		}
		assert.Contains(t, allErrorsText, errorStr)
	}
	testSingleFailure := func(profile Profile, errorStr string) {
		testFailure(profile, 1, errorStr)
	}

	// this profile will be built up from failing to eventually passing
	currentOS := getCurrentOs()
	profile := Profile{
		IsDisabled: true,
	}
	testSingleFailure(profile, "Profile is disabled, doing nothing")
	profile.IsDisabled = false
	profile.OS = getOtherOs(currentOS)
	testSingleFailure(profile, "this profile is not supported on this OS, doing nothing.")
	profile.OS = currentOS
	// testSingleFailure(profile, "unable to create post scripts: no post scripts specified")

	profile.NamePatterns = []ParameterizedText{}
	profile.Type = FileType
	testSingleFailure(profile, "couldn't find a good new name")

	profile.NamePatterns = []ParameterizedText{"file1"}
	profile.RunPostscriptOnFailure = true
	testSingleFailure(profile, "couldn't find a good new name")

	profile.NamePatterns = []ParameterizedText{"file3"}
	profile.PostScripts = []ParameterizedText{"echo {{.ProjectName}}-{{.Year}}"}
	profile.TargetDirectory = "./test/dir"
	testSuccess(profile)
	assert.Equal(t, 1, len(monkeyPatchCalls.ExecCommandCalls), "calls to ExecCommand")
	assert.Equal(t, 1, len(monkeyPatchCalls.OSChdirCalls), "calls to OSChdir")
	assert.Equal(t, 1, len(monkeyPatchCalls.OSGetwdCalls), "calls to OSGetwd")
	assert.Equal(t, 1, len(monkeyPatchCalls.OSReadDirCalls), "calls to OSReadDir")
	assert.Equal(t, 1, len(monkeyPatchCalls.OSWriteFileCalls), "calls to OSWriteFile")

	profile.PostScripts = []ParameterizedText{"echo ''"}
	testSuccess(profile)
	assert.Equal(t, 1, len(monkeyPatchCalls.ExecCommandCalls), "calls to ExecCommand")
	assert.Equal(t, 1, len(monkeyPatchCalls.OSChdirCalls), "calls to OSChdir")
	assert.Equal(t, 1, len(monkeyPatchCalls.OSGetwdCalls), "calls to OSGetwd")
	assert.Equal(t, 1, len(monkeyPatchCalls.OSReadDirCalls), "calls to OSReadDir")
	assert.Equal(t, 1, len(monkeyPatchCalls.OSWriteFileCalls), "calls to OSWriteFile")
}

func getCurrentOs() OSType {
	os := runtime.GOOS
	if os == "windows" {
		return "Windows"
	} else {
		return "Mac"
	}
}
func getOtherOs(currentOs OSType) OSType {
	if currentOs == "Windows" {
		return "Mac"
	}
	return "Windows"
}
