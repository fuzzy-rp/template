package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
)

func TestExtras(t *testing.T) {
	// setup code
	extras := map[string]string{
		"version":  "1",
		"id":       "123",
		"customer": "abc",
	}

	file, err := os.CreateTemp("", "extras.yaml")
	if err != nil {
		t.Error("Error creating temp file: ", err)
	}

	// cleanup code
	deleteFile := func() {
		os.Remove(file.Name())
	}
	defer deleteFile()

	// write extras to file
	encoder := yaml.NewEncoder(file)
	err = encoder.Encode(extras)
	if err != nil {
		t.Error("Error encoding extras: ", err)
	}

	// close file
	err = file.Close()
	if err != nil {
		t.Error("Error closing file: ", err)
	}

	// file name
	fileName := file.Name()

	// happy path
	loadedExtras, err := loadExtras(fileName)
	if err != nil {
		t.Error("Error loading extras: ", err)
	}
	assert.Equal(t, extras, loadedExtras)

	// test path errors
	_, err = loadExtras("non-existent-file.yaml")
	assert.NotNil(t, err, "Expected error loading non-existent file")

	err = os.WriteFile(file.Name(), []byte("invalid yaml"), 0644)
	assert.Nil(t, err, "Error writing invalid yaml file")
	_, err = loadExtras(fileName)
	assert.NotNil(t, err, "Expected to fail to parse extras, but succeeded")
}
