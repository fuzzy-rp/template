package main

import (
	"fmt"
)

func createPostScriptArrays(
	profile Profile,
	postPatternData PostPatternData) ([]string, error) {
	array := make([]string, 0)

	if profile.PostScripts == nil || len(profile.PostScripts) == 0 {
		return nil, fmt.Errorf("no post scripts specified")
	}

	for _, postScriptPattern := range profile.PostScripts {
		postScript, err := parseAndExecTemplate(newTemplate(), postScriptPattern, postPatternData)
		if err != nil {
			return nil, fmt.Errorf("error parsing post script: %w", err)
		}

		array = append(array, postScript)
	}

	return array, nil
}
