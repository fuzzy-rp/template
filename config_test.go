package main

import (
	"errors"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Compares YAML representation of two objects
func compareObjectYaml(t *testing.T, expected, actual interface{}) {
	expectedYaml, err := getYaml(expected)
	assert.Nil(t, err, "Error marshalling expected")
	actualYaml, err := getYaml(actual)
	assert.Nil(t, err, "Error marshalling actual")
	assert.Equal(t, len(expectedYaml), len(actualYaml), "Expected YAML and actual YAML are different lengths")
	//assert.Equal(t, expectedYaml, actualYaml)
}

func TestSaveLoadConfig(t *testing.T) {
	const configPath string = "./test/test-config.yaml"
	configDir := filepath.Dir(configPath)
	err := os.Mkdir(configDir, 0755)
	if err != nil && !errors.Is(err, os.ErrExist) {
		assert.Nil(t, err, "Error creating config directory")
	}
	defer os.Remove(configDir)

	config := defaultConfig()

	// save config
	err = saveConfig(configPath, config)
	assert.Nil(t, err, "Error saving config")

	// load config
	loadedConfig, err := loadConfig(configPath)
	assert.Nil(t, err, "Error loading config")

	// test config
	compareObjectYaml(t, loadedConfig, config)

	// test load non-existent config
	_, err = loadConfig("./test/non-existent-file.yaml")
	assert.NotNil(t, err, "Expected to fail to load config, but succeeded")

	// test load invalid yaml
	err = os.WriteFile("./invalid-yaml.yaml", []byte("invalid yaml"), 0644)
	assert.Nil(t, err, "Error writing invalid yaml file")
	_, err = loadConfig("./invalid-yaml.yaml")
	assert.NotNil(t, err, "Expected to fail to parse config, but succeeded")
	os.Remove("./invalid-yaml.yaml")

	// test saving config to invalid path
	err = saveConfig("invalid/path/invalid-config.yaml", config)
	assert.NotNil(t, err, "Expected to fail to save config, but succeeded")

	// remove config
	os.Remove(configPath)

	// test default config creation
	_, err = os.Stat(configPath)
	assert.True(t, errors.Is(err, os.ErrNotExist), "Expected config file to not exist, but it does")

	// fail to get config
	_, err = loadConfig(configPath)
	assert.NotNil(t, err, "Expected to fail to load config, but succeeded")

	// create default config
	err = saveConfig(configPath, defaultConfig())
	assert.Nil(t, err, "Error saving config")

	// test config
	compareObjectYaml(t, loadedConfig, config)

	// verify config file exists
	stat1, err := os.Stat(configPath)
	assert.Nil(t, err, "Expected config file to exist, but it does not")

	// get and create config
	loadedConfig, err = loadConfig(configPath)
	assert.Nil(t, err, "Error loading config")

	// test config
	compareObjectYaml(t, loadedConfig, config)

	// verify config file wasn't updated
	stat2, err := os.Stat(configPath)
	assert.Nil(t, err, "Expected config file to exist, but it does not")
	assert.Equal(t, stat1.ModTime(), stat2.ModTime(), "Expected config file to not be modified, but it was")
}

func TestValidateDefaultConfig(t *testing.T) {
	config := defaultConfig()
	err := validateConfig(&config)
	if err != nil {
		t.Errorf("Expected to validate config, but failed: %v", err)
	}
}

func TestValidateConfig(t *testing.T) {
	testFailure := func(config Config, errorText string) {
		err := validateConfig(&config)
		if err == nil {
			t.Errorf("Expected to fail to validate config, but succeeded")
		}
		assert.Contains(t, err.Error(), errorText)
	}

	testFailure(Config{}, "no profiles found")
	testFailure(Config{Profiles: []Profile{}}, "no profiles found")
	testFailure(Config{Profiles: []Profile{
		{
			Name:         "daily",
			IsDefault:    true,
			NamePatterns: []ParameterizedText{"daily"},
			FileTemplate: "foo",
		},
	}}, "error validating profile")
	testFailure(Config{Profiles: []Profile{
		{
			Name:            "daily",
			IsDefault:       true,
			NamePatterns:    []ParameterizedText{"daily"},
			Type:            FileType,
			TargetDirectory: "foo",
			FileTemplate:    "foo",
		},
		{
			Name:            "daily2",
			IsDefault:       true,
			NamePatterns:    []ParameterizedText{"daily"},
			Type:            FileType,
			TargetDirectory: "foo",
			FileTemplate:    "foo",
		},
	}}, "multiple default profiles found")
	testFailure(Config{Profiles: []Profile{
		{
			Name:            "daily",
			IsDefault:       true,
			NamePatterns:    []ParameterizedText{"daily"},
			Type:            FileType,
			TargetDirectory: "foo",
			FileTemplate:    "foo",
		},
		{
			Name:            "daily",
			IsDefault:       true,
			NamePatterns:    []ParameterizedText{"daily"},
			Type:            FileType,
			TargetDirectory: "foo",
			FileTemplate:    "foo",
		},
	}}, "duplicate profile name 'daily'")
}

func TestLoadConfig(t *testing.T) {
	bytes := []byte(`
profiles:
    - name:
`)
	err := os.WriteFile("./invalid-config.yaml", bytes, 0644)
	assert.Nil(t, err, "Error writing invalid config file")
	defer os.Remove("./invalid-config.yaml")

	// load config
	_, err = loadConfig("./invalid-config.yaml")
	assert.NotNil(t, err, "Expected to fail to load config, but succeeded")
	assert.Contains(t, err.Error(), "error validating profile : profile name is required")
}

func TestSaveConfig(t *testing.T) {
	config := Config{Profiles: []Profile{}}
	err := saveConfig("./invalid-config.yaml", config)
	assert.NotNil(t, err, "Expected to fail to save config, but succeeded")
	assert.Contains(t, err.Error(), "no profiles found")
}

func TestMergeConfigs(t *testing.T) {
	defaultConfig := defaultConfig()
	testSuccess := func(config1 Config, config2 Config) Config {
		config, err := mergeConfigs(config1, config2)
		// compareObjectYaml(t, mergedConfig, expected)
		assert.Nil(t, err, "Expected to merge configs, but failed")
		return config
	}
	testFailure := func(config1 Config, config2 Config, errorText string) {
		_, err := mergeConfigs(config1, config2)
		assert.NotNil(t, err, "Expected to fail to merge configs, but succeeded")
		assert.Contains(t, err.Error(), errorText)
	}
	testFailure(Config{}, Config{}, "no profiles found")
	testFailure(Config{Profiles: []Profile{{}}}, Config{}, "error validating config a: error validating profile : profile name is required")
	testFailure(Config{Profiles: []Profile{defaultConfig.Profiles[0]}}, Config{}, "error validating config b: no profiles found")

	profile1 := defaultConfig.Profiles[0]
	profile2 := defaultConfig.Profiles[1]
	testSuccess(Config{Profiles: []Profile{profile1}}, Config{Profiles: []Profile{profile2}})

	profile1_updated := profile1
	profile1_updated.Description += " This profile has been updated!"

	result := testSuccess(Config{Profiles: []Profile{profile1}}, Config{Profiles: []Profile{profile1_updated}})
	assert.Equal(t, 1, len(result.Profiles))
	assert.Equal(t, profile1_updated.Name, result.Profiles[0].Name)
}
