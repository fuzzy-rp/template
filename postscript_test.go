package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreatePostScriptArrays(t *testing.T) {
	callCreatePostScriptArrays := func(postScripts []ParameterizedText) ([]string, error) {
		return createPostScriptArrays(
			Profile{PostScripts: postScripts},
			PostPatternData{
				FullPath: "test",
				PrePatternData: PrePatternData{
					Extras: map[string]string{"foo": "bar"},
				},
			})
	}

	arr, err := callCreatePostScriptArrays([]ParameterizedText{"echo \"{{.FullPath}}\"-{{.Extras.foo}} "})
	assert.Nil(t, err, "Error creating postscript array")
	assert.Equal(t, 1, len(arr))
	assert.Equal(t, arr[0], "echo \"test\"-bar ")

	arr, err = callCreatePostScriptArrays(nil)
	assert.NotNil(t, err, "nil postscripts should fail")
	assert.Nil(t, arr)

	arr, err = callCreatePostScriptArrays([]ParameterizedText{})
	assert.NotNil(t, err, "empty postscripts should fail")
	assert.Nil(t, arr)

	arr, err = callCreatePostScriptArrays([]ParameterizedText{"echo \"{{.Name"})
	assert.NotNil(t, err, "invalid template should fail")
	assert.Nil(t, arr)

	arr, err = callCreatePostScriptArrays([]ParameterizedText{"echo \"{{.NameUnknown}}\""})
	assert.NotNil(t, err, "invalid template variable should fail")
	assert.Nil(t, arr)
}
