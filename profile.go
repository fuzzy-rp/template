package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"golang.org/x/exp/slices"
)

type ProfileType string

const (
	FileType   ProfileType = "file"
	FolderType ProfileType = "folder"
	ScriptType ProfileType = "script"
)

var allProfileTypes = []ProfileType{
	FileType,
	FolderType,
	ScriptType,
}

type OSType string

const (
	Windows OSType = "Windows"
	Mac     OSType = "Mac"
	All     OSType = "All"
)

type ParameterizedText string

type FileInFolder struct {
	Name         ParameterizedText
	FileTemplate ParameterizedText `yaml:"file,omitempty"`
}

type Profile struct {
	Name                   string
	Description            string `yaml:"description,omitempty"`
	IsDefault              bool
	IsDisabled             bool
	Type                   ProfileType
	OS                     OSType              `yaml:"os,omitempty"`
	RunPostscriptOnFailure bool                `yaml:"runpostscriptonfailure,omitempty"`
	PostScripts            []ParameterizedText `yaml:"postscripts,omitempty"`

	NamePatterns []ParameterizedText

	TargetDirectory string            `yaml:"targetdirectory,omitempty"`
	FileTemplate    ParameterizedText `yaml:"filetemplate,omitempty"`
	Files           []FileInFolder    `yaml:"files,omitempty"`
}

var forbiddenProfileNames = []string{
	"list",
	"get",
}

func processProfile(profile Profile, projectName string, extras map[string]string, usr user.User, pipeInputScanner *bufio.Scanner) []error {
	if profile.IsDisabled {
		return []error{fmt.Errorf("Profile is disabled, doing nothing")}
	}

	// decide if the profile should run on this OS
	profileOs := profile.OS
	isWindows := runtime.GOOS == "windows"
	shouldRun := profileOs == "" ||
		profileOs == "All" ||
		(profileOs == "Windows" && isWindows) ||
		(profileOs == "Mac" && !isWindows)
	if !shouldRun {
		return []error{
			fmt.Errorf("this profile is not supported on this OS, doing nothing. Profile OS: %s, system OS: %s. Supported OS values: Windows, Mac, All", profileOs, runtime.GOOS),
		}
	}

	// determine if we're dealing with files or directories
	profileType := profile.Type

	cwd, err := monkey.OSGetwd()
	if err != nil {
		return []error{fmt.Errorf("unable to get current working directory: %v", err)}
	}
	if profile.TargetDirectory != "" {
		path := getFullPath(profile.TargetDirectory, usr)

		if _, err := monkey.OSStat(path); errors.Is(err, os.ErrNotExist) {
			err = monkey.OSMkdir(path, 0755)
			if err != nil {
				return []error{fmt.Errorf("unable to create target directory: %v", err)}
			}
		}

		// change cwd to the profile's path, if one is specified
		err = monkey.OSChdir(path)
		if err != nil {
			return []error{fmt.Errorf("unable to change directory (1) to %v: %v", path, err)}
		}
		cwd = path
	}

	errors := []error{}
	fullPath := ""
	now := time.Now()
	prePatternData := createPrePatternData(now, projectName, extras)

	if profileType == FileType || profileType == FolderType {
		if pipeInputScanner != nil && profileType != FileType {
			errors = append(errors, fmt.Errorf("cannot use pipe input with non-file type profile"))
			return errors
		}

		// get all entities in current directory
		files, err := monkey.OSReadDir(cwd)
		if err != nil {
			return []error{err}
		}

		// filter entities to just files or directories
		var entities []string
		for _, file := range files {
			isDir := file.IsDir()
			if isDir == (profileType == FolderType) {
				entities = append(entities, file.Name())
			}
		}

		// generate a new name based on time, profile, and existing entities
		validator := func(newName string) bool { return !slices.Contains(entities, newName) }
		newName, err := createNewNameForPatterns(now, prePatternData, projectName, profile.NamePatterns, extras, validator)
		swallowPrePostScriptErrors := profile.RunPostscriptOnFailure && profile.PostScripts != nil && len(profile.PostScripts) > 0

		// check newName is created
		if newName == "" {
			if swallowPrePostScriptErrors {
				fmt.Println("No new name was created, but will attempt to run post scripts")
			} else {
				errors = append(errors, fmt.Errorf("couldn't find a good new name"))
				return errors
			}
		}

		if err == nil && newName != "" {
			fullPath, err = createEntity(now, profile, cwd, newName, projectName, extras, prePatternData, pipeInputScanner)
			if err != nil {
				if swallowPrePostScriptErrors {
					fmt.Printf("Error creating entity, but will attempt to run post scripts: %v", err)
				} else {
					errors = append(errors, fmt.Errorf("unable to create entity: %v", err))
					return errors
				}
			}
		}
	}

	// execute PostScript, if present
	if profile.PostScripts != nil && len(profile.PostScripts) > 0 {
		postPatternData := createPostPatternData(fullPath, prePatternData)
		postScripts, err := createPostScriptArrays(profile, postPatternData)
		if err != nil {
			errors = append(errors, fmt.Errorf("unable to create post scripts: %v", err))
			return errors
		}
		for _, postScript := range postScripts {

			shell, args := shellCommandAndArgs(postScript)
			var out bytes.Buffer
			fmt.Printf("Executing post script: %v %v\n", shell, strings.Join(args, " "))
			cmd := monkey.ExecCommand(shell, args...)
			cmd.Stdout = &out
			err := cmd.Run()
			output := out.String()
			if output != "" {
				fmt.Printf("Post script output: %v\n", output)
			} else {
				fmt.Printf("Post script had no output\n")
			}
			if err != nil {
				// post scripts can fail (TODO: investigate why), so for now we'll just log the error

				fmt.Printf("Error executing post script '%v': %v\n",
					fmt.Sprintf("%v %v", shell, strings.Trim(fmt.Sprint(args), "[]")), err)
			}
		}
	}

	return nil
}

func shellCommandAndArgs(command string) (shell string, args []string) {
	if runtime.GOOS == "windows" {
		shell = "cmd"
		args = []string{"/c", command}
		return
	} else {
		shell = "sh"
		args = []string{"-c", command}
		return
	}
}

func createEntity(
	now time.Time,
	profile Profile,
	cwd string,
	newName string,
	projectName string,
	extras map[string]string,
	prePatternData PrePatternData,
	pipeInputScanner *bufio.Scanner) (string, error) {

	profileType := profile.Type
	fullPath := filepath.Join(cwd, newName)
	if profileType == FolderType {
		fmt.Printf("Creating folder: %v\n", fullPath)
		err := monkey.OSMkdir(fullPath, os.ModePerm)
		if err != nil {
			return "", fmt.Errorf("error creating directory: %v", err)
		}
		err = monkey.OSChdir(fullPath)
		if err != nil {
			return "", fmt.Errorf("unable to change directory (2) to %v: %v", fullPath, err)
		}

		// create files in the new folder
		if len(profile.Files) > 0 {
			fmt.Printf("Creating %v files in folder: %v\n", len(profile.Files), fullPath)
			existingFiles := []string{}
			for index, file := range profile.Files {
				filePrePatternData := createPrePatternData(now, projectName, extras)
				validator := func(newName string) bool { return !slices.Contains(existingFiles, newName) }
				fileName, err := createNewNameForPatterns(now, filePrePatternData, projectName, []ParameterizedText{file.Name}, extras, validator)
				if err != nil {
					return "", fmt.Errorf("error creating file name for file index %v : %v", index, err)
				}
				existingFiles = append(existingFiles, fileName)
				subFilePath := filepath.Join(fullPath, fileName)
				subDirPath := filepath.Dir(subFilePath)

				if _, err := os.Stat(subDirPath); os.IsNotExist(err) {
					fmt.Printf("Creating subdirectory: %v\n", subDirPath)
					err = monkey.OSMkdir(subDirPath, os.ModePerm)
					if err != nil {
						return "", fmt.Errorf("error creating sub directory for file index %v : %v", index, err)
					}
				}
				fmt.Printf("Creating sub file: %v\n", subFilePath)
				err = createFile(filePrePatternData, file.FileTemplate, subFilePath, nil)
				if err != nil {
					return "", fmt.Errorf("error creating file %v: %v", subFilePath, err)
				}
			}
		}
	} else if profileType == FileType {
		fmt.Printf("Creating file: %v\n", fullPath)
		err := createFile(prePatternData, profile.FileTemplate, fullPath, pipeInputScanner)
		if err != nil {
			return "", err
		}
	} else {
		fmt.Printf("Profile type is script, doing nothing during main phase\n")
	}
	return fullPath, nil
}

func validateProfile(profile *Profile) error {
	// check name
	if profile.Name == "" {
		return fmt.Errorf("profile name is required")
	}
	if slices.Contains(forbiddenProfileNames, profile.Name) {
		return fmt.Errorf("forbidden profile name '%s'", profile.Name)
	}

	// check name patterns
	if profile.NamePatterns == nil || len(profile.NamePatterns) == 0 {
		return fmt.Errorf("namePatterns is required")
	}
	for index, namePattern := range profile.NamePatterns {
		if namePattern == "" {
			return fmt.Errorf("name pattern is required for pattern index %v of profile %v", index, profile.Name)
		}
	}

	// check type
	if profile.Type == "" || !slices.Contains(allProfileTypes, profile.Type) {
		return fmt.Errorf("invalid profile type %s for profile %s", profile.Type, profile.Name)
	}

	// check FileType profile
	if profile.Type == FileType {
		if profile.TargetDirectory == "" {
			return fmt.Errorf("targetDirectory is not specified for profile %s, which has Type=%s", profile.Name, profile.Type)
		}
	}

	// check FolderType profile
	if profile.Type == FolderType {
		if profile.TargetDirectory == "" {
			return fmt.Errorf("targetDirectory is not specified for profile %s, which has Type=%s", profile.Name, profile.Type)
		}
	}

	// check ScriptType profile
	if profile.Type == ScriptType {
		if profile.NamePatterns != nil && len(profile.NamePatterns) > 1 {
			return fmt.Errorf("multiple name patterns are not supported for profile %s, which has Type=%s", profile.Name, profile.Type)
		}
	}

	// set files to empty array if nil
	if profile.Files == nil {
		profile.Files = []FileInFolder{}
	}

	return nil
}
