.PHONY: test

BINARY=template
VERSION=$(shell git describe --tags)
LDFLAGS=-ldflags "-w -s -X main.Version=${VERSION}"

all: build test lint
all-clean: clean all

dev-setup:
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest
	golangci-lint --version
	go install github.com/goreleaser/goreleaser@latest
	goreleaser --version
	echo "${VERSION}" > dev-setup

prep: dev-setup
	go mod tidy

clean:
	go clean
	go mod tidy
	rm -f dev-setup

build: prep
	go build ${LDFLAGS} -o ${BINARY} .

lint: build
	golangci-lint run --timeout=10m

test: build
	go test -v ./... -coverprofile="./test-coverage.out"

test_coverage: test
	go tool cover -html="./test-coverage.out" -o "./test-coverage.html"

test_coverage_html: test_coverage
	open "./test-coverage.html"

install: build test lint
	go install ${LDFLAGS}
	template reset

uninstall:
	go clean -i gitlab.com/PavelSafronov/template...
	rm -f $(BINARY)

release: all
	git restore go.mod
	goreleaser release --clean

brew-install:
	brew tap PavelSafronov/template https://gitlab.com/PavelSafronov/template.git
	brew install template

brew-uninstall:
	brew uninstall template
	brew untap PavelSafronov/template
