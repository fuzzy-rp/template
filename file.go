package main

import (
	"bufio"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
)

func createFile(
	prePatternData PrePatternData,
	fileTemplate ParameterizedText,
	fullPath string,
	pipeInputScanner *bufio.Scanner) error {

	useTemplate := fileTemplate != ""
	useInputPipe := pipeInputScanner != nil

	if useInputPipe {
		log.WithFields(log.Fields{"fullPath": fullPath}).Debug("Creating file from input pipe content", fullPath)

		file, err := monkey.OSCreate(fullPath)
		if err != nil {
			return fmt.Errorf("error creating file: %v", err)
		}
		defer file.Close()

		writer := bufio.NewWriter(file)
		for pipeInputScanner.Scan() {
			line := pipeInputScanner.Text()
			_, err := writer.WriteString(line + "\n")
			if err != nil {
				return fmt.Errorf("error writing file: %v", err)
			}
		}
		writer.Flush()
	} else if useTemplate {
		postPatternData := createPostPatternData(fullPath, prePatternData)
		log.WithFields(log.Fields{"fullPath": fullPath}).Debug("Creating file from template string\n", fullPath)

		templateString, err := parseAndExecTemplate(newTemplate(), fileTemplate, postPatternData)
		if err != nil {
			return fmt.Errorf("error creating templated file: %v", err)
		}

		err = monkey.OSWriteFile(fullPath, []byte(templateString), os.ModePerm)
		if err != nil {
			return fmt.Errorf("error writing file: %v", err)
		}
	} else {
		log.WithFields(log.Fields{"fullPath": fullPath}).Debug("Creating empty file", fullPath)
		err := monkey.OSWriteFile(fullPath, []byte{}, os.ModePerm)
		if err != nil {
			return fmt.Errorf("error writing file: %v", err)
		}
	}
	return nil
}

func readFile(
	filePath string,
) (string, error) {
	file, err := os.ReadFile(filePath)
	if err != nil {
		return "", fmt.Errorf("error reading file %s: %w", filePath, err)
	}
	return string(file), nil
}

