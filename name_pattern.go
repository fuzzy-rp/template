package main

import (
	"errors"
	"fmt"
	"text/template"
	"time"
)

func createNewNameForPatterns(
	now time.Time,
	patternData PrePatternData,
	projectName string,
	namePatterns []ParameterizedText,
	extras map[string]string,
	validator func(newName string) bool) (string, error) {
	for index, namePattern := range namePatterns {

		startingVersion := 1
		invokeCount := 0
		funcMap := template.FuncMap{
			"getVersion": func() int {
				invokeCount++
				return startingVersion
			},
		}
		t := newTemplate()
		t = t.Funcs(funcMap)

		for {

			newName, err := parseAndExecTemplate(t, namePattern, patternData)
			if err != nil {
				return "", fmt.Errorf("error templating name pattern %d: %w", index, err)
			}

			isValid := validator(newName)
			useIndex := invokeCount > 0
			isLastPattern := index == len(namePatterns)-1

			// if new name is valid, use this name
			if isValid {
				return newName, nil
			}

			// if index is not being used (template did not invoke getVersion)
			// then we're done
			if !useIndex {
				break
			}

			// if getVersion was used, but this isn't the last pattern, report an error
			if useIndex && !isLastPattern {
				return "", errors.New("cannot use function getVersion in a pattern that is not the last pattern")
			}

			startingVersion++
		}
	}
	return "", errors.New("couldn't find a new name")
}
