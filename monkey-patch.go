package main

import (
	"os"
	"os/exec"
	"testing"

	"github.com/stretchr/testify/assert"
)

type monkeyPatch struct {
	ExecCommand func(name string, arg ...string) *exec.Cmd
	OSCreate    func(name string) (*os.File, error)
	OSWriteFile func(filename string, data []byte, perm os.FileMode) error
	OSGetwd     func() (string, error)
	OSChdir     func(dir string) error
	OSMkdir     func(path string, perm os.FileMode) error
	OSReadDir   func(dirname string) ([]os.DirEntry, error)
	OSStat      func(name string) (os.FileInfo, error)
}
type monkeyPatchCalls struct {
	ExecCommandCalls []struct {
		name string
		arg  []string
	}
	OSCreateCalls []struct {
		name string
	}
	OSWriteFileCalls []struct {
		filename string
		data     []byte
		perm     os.FileMode
	}
	OSGetwdCalls []struct{}
	OSChdirCalls []struct{ dir string }
	OSMkdirCalls []struct {
		path string
		perm os.FileMode
	}
	OSReadDirCalls []struct{ dirname string }
	OSStatCalls    []struct{ name string }
}

var monkey = defaultMonkeyPatch()

func applyTestMonkeyPatch(t *testing.T, monkeyImpl monkeyPatch) *monkeyPatchCalls {
	t.Cleanup(undoMonkeyPatch)
	results := monkeyPatchCalls{
		ExecCommandCalls: []struct {
			name string
			arg  []string
		}{},
		OSCreateCalls: []struct {
			name string
		}{},
		OSWriteFileCalls: []struct {
			filename string
			data     []byte
			perm     os.FileMode
		}{},
		OSGetwdCalls: []struct{}{},
		OSChdirCalls: []struct{ dir string }{},
		OSMkdirCalls: []struct {
			path string
			perm os.FileMode
		}{},
		OSReadDirCalls: []struct{ dirname string }{},
		OSStatCalls:    []struct{ name string }{},
	}
	monkey = monkeyPatch{
		ExecCommand: func(name string, arg ...string) *exec.Cmd {
			assert.NotNil(t, monkeyImpl.ExecCommand, "ExecCommand is not implemented")
			results.ExecCommandCalls = append(results.ExecCommandCalls, struct {
				name string
				arg  []string
			}{name, arg})
			return monkeyImpl.ExecCommand(name, arg...)
		},
		OSCreate: func(name string) (*os.File, error) {
			assert.NotNil(t, monkeyImpl.OSCreate, "OSCreate is not implemented")
			results.OSCreateCalls = append(results.OSCreateCalls, struct {
				name string
			}{name})
			return monkeyImpl.OSCreate(name)
		},
		OSWriteFile: func(filename string, data []byte, perm os.FileMode) error {
			assert.NotNil(t, monkeyImpl.OSWriteFile, "OSWriteFile is not implemented")
			results.OSWriteFileCalls = append(results.OSWriteFileCalls, struct {
				filename string
				data     []byte
				perm     os.FileMode
			}{filename, data, perm})
			return monkeyImpl.OSWriteFile(filename, data, perm)
		},
		OSGetwd: func() (string, error) {
			assert.NotNil(t, monkeyImpl.OSGetwd, "OSGetwd is not implemented")
			results.OSGetwdCalls = append(results.OSGetwdCalls, struct{}{})
			return monkeyImpl.OSGetwd()
		},
		OSChdir: func(dir string) error {
			assert.NotNil(t, monkeyImpl.OSChdir, "OSChdir is not implemented")
			results.OSChdirCalls = append(results.OSChdirCalls, struct{ dir string }{dir})
			return monkeyImpl.OSChdir(dir)
		},
		OSMkdir: func(path string, perm os.FileMode) error {
			assert.NotNil(t, monkeyImpl.OSMkdir, "OSMkdir is not implemented")
			results.OSMkdirCalls = append(results.OSMkdirCalls, struct {
				path string
				perm os.FileMode
			}{path, perm})
			return monkeyImpl.OSMkdir(path, perm)
		},
		OSReadDir: func(dirname string) ([]os.DirEntry, error) {
			assert.NotNil(t, monkeyImpl.OSReadDir, "OSReadDir is not implemented")
			results.OSReadDirCalls = append(results.OSReadDirCalls, struct{ dirname string }{dirname})
			return monkeyImpl.OSReadDir(dirname)
		},
		OSStat: func(name string) (os.FileInfo, error) {
			assert.NotNil(t, monkeyImpl.OSStat, "OSStat is not implemented")
			results.OSStatCalls = append(results.OSStatCalls, struct{ name string }{name})
			return monkeyImpl.OSStat(name)
		},
	}
	return &results
}

func defaultMonkeyPatch() monkeyPatch {
	return monkeyPatch{
		ExecCommand: exec.Command,
		OSCreate:    os.Create,
		OSWriteFile: os.WriteFile,
		OSGetwd:     os.Getwd,
		OSChdir:     os.Chdir,
		OSMkdir:     os.Mkdir,
		OSReadDir:   os.ReadDir,
		OSStat:      os.Stat,
	}
}

func undoMonkeyPatch() {
	monkey = defaultMonkeyPatch()
}
