package main

import (
	"os/user"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetFullPath(t *testing.T) {
	usr, err := user.Current()
	assert.Nil(t, err)

	testSuccess := func(path string, expected string) {
		fullPath := getFullPath(path, *usr)
		if fullPath != expected {
			t.Errorf("Expected to get full path '%v' for '%v', but got '%v'", expected, path, fullPath)
		}
	}
	// testFailure := func(path string) {
	// 	_, err := getFullPath(path)
	// 	if err == nil {
	// 		t.Errorf("Expected to fail to get full path for '%v', but succeeded", path)
	// 	}
	// }

	testSuccess("C:\\", "C:\\")
	testSuccess("C:\\test", "C:\\test")
	testSuccess("C:\\test\\", "C:\\test\\")
	testSuccess("C:\\test\\..", "C:\\test\\..") // WRONG

	testSuccess("~/", usr.HomeDir)
	testSuccess("~", usr.HomeDir)
}
