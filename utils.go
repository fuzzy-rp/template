package main

import (
	"os/user"
	"path/filepath"
	"strings"

	"gopkg.in/yaml.v3"
)

func getFullPath(path string, usr user.User) string {
	dir := usr.HomeDir
	var newPath string
	if path == "~" {
		newPath = dir
	} else if strings.HasPrefix(path, "~/") {
		newPath = filepath.Join(dir, path[2:])
	} else {
		newPath = path
	}
	return newPath
}

func getYaml(data interface{}) (string, error) {
	yaml, err := yaml.Marshal(data)
	if err != nil {
		return "", err
	}
	return string(yaml), nil
}
