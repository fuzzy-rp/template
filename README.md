# README

## Name

template

## Description

template is a command-line interface (CLI) tool that automates the creation of files and folders with pre-defined names and contents, so you can focus on your work and not on repetitive setup tasks.

As developers and content creators, we often need to create files and folders with specific naming conventions, such as time-stamped file names, project-specific prefixes, and consistent suffixes. We also frequently need to populate these files with pre-defined contents, such as boilerplate code or templates for documentation. These tasks can be tedious and error-prone when done manually, especially when working on multiple projects or collaborating with others.

template simplifies these tasks by providing a flexible and customizable way to create files and folders with consistent naming and content. With template, you can define your own naming patterns, file templates, and directory structures, or use pre-configured profiles that come with the tool. You can also pass in parameters at runtime to customize the output further, such as including a project name or arbitrary template data.

template is built in Go and uses YAML and sprig templates for configuration. It supports creating both files and directories from predefined templates, as well as piped input for quick creation of files with custom content. With template, you can spend more time on your work and less time on setup and organization.

## Usage

To use template, you can run the template command followed by a profile name, parameters, and options. For example:

```sh
template daily "Cool Project"
```

This will create a new file in the format YYYY-MM-DD-cool_project.md in the ~/DailyNotes directory, with pre-defined contents.

You can also define your own profiles in the ~/.template.yaml file, or specify file, directory, and script templates directly at runtime using the `-f`, `-d`, and `-s` options.

### Sample tool config

This is one of the profiles that comes with the tool.

```yaml
    - name: daily
      description: Creates a MD file in ~/DailyNotes folder in the format YYYY-MM-DD[-vX].md
      isdefault: false
      isdisabled: false
      type: file
      runpostscriptonfailure: true
      postscripts:
        - code . {{ if .FullPath }}"{{.FullPath}}"{{else}}(ls -t | head -n1){{ end }}
      namepatterns:
        - '{{$version := getVersion}}{{.YYYY_MM_DD}}{{if .ProjectName}}-{{.ProjectName | snakecase }}{{end}}{{if (gt $version 1)}}v{{$version}}{{end}}.md'
      targetdirectory: ~/DailyNotes
      filetemplate: |-
        # {{.FileNameWithoutExtension}}

        Today's notes for {{.YYYY_MM_DD}}.

        ## Todo

        1. Check email
        2. Check chat
        3. Check calendar
        4. Check yesterday's notes
        5. Have fun!
        6. ADD MORE ITEMS HERE
```

Using the above configuration:

```sh
> $ template daily "Cool Thought"
Profile: daily
Project name: Cool Thought
Loaded config from /Users/pavel/.template.yaml
Loaded user config from /Users/pavel/.template.user.yaml, combining with global config
Creating file: /Users/pavel/DailyNotes/2022-11-20-cool_thought.md
Executing post script: sh -c code . "/Users/pavel/DailyNotes/2022-11-20-cool_thought.md"
Post script had no output
```

This will cause VSCode to open the `~\DailyNotes` folder as a workspace, and the newly-created file `2022-11-20-cool_thought.md` will be opened as well.

The above example also uses the [`snakecase`](https://github.com/Masterminds/sprig/blob/master/docs/strings.md#user-content-snakecase) template function.
This is one of the [sprig template functions](https://github.com/Masterminds/sprig/blob/master/docs/index.md) that are supported by template.

File-based profiles (`type: file`) can accept piped input, and this input will be piped to the created file.
In this instance, the profile's `FileTemplate` field is ignored.
Here's an example of piping the results of `ps` to a file with an auto-generated name:

```sh
> $ echo "$(ps)" | template log "ps-info"
Profile: log
Project name: ps-info
Loaded config from /Users/pavel/.template.yaml
Loaded user config from /Users/pavel/.template.user.yaml, combining with global config
Creating file: 2022-12-10-00-05-21-ps-info.log
Executing post script: sh -c code . 2022-12-10-00-05-21-ps-info.log
Post script had no output
```

Note that user profiles are combined with the default ones.
This is so you can run `template reset` to reset `~/.template.yaml`, but keep your local profiles/changes as-is.

### Using a file or folder template directly

You can also specify a file or folder template during execution, without changing any configs.

Creating a file:

```sh
template -f "{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}{{if .ProjectName}}-{{.ProjectName}}{{end}}.txt" "project name"
```

Creating a folder:

```sh
template -d "{{.Year}}-{{.Month}}-{{.Day}}-{{.ProjectName}}.txt" Test123
```

### Using a script template directly

You can also specify a template script:

```sh
template --script "echo 'Hello from {{.YYYY_MM_DD}}!'"
```

will result in:

```txt
> template --script "echo 'Hello from {{.YYYY_MM_DD}}!'"
Project name: 
Loaded config from C:\Users\pavel\.template.yaml
Loaded user config from C:\Users\pavel\.template.user.yaml, combining with global config
Executing post script: cmd /c echo 'Hello from 2023-04-02!'
Post script output: 'Hello from 2023-04-02!'

```

## Examples

### Use `daily` profile to create a new file

```sh
template daily
```

### Use `daily` profile to create a new file with a project name

```sh
template daily testing
```

### Use `daily` profile to create a new file with a project name that contains spaces

```sh
template daily "this is a test"
```

### Use `daily` profile to create a new file with contents piped in from `ps`

```sh
echo "$(ps)" | template daily
```

### Use `work` profile to create a folder with multiple files inside it

```sh
template work
```

### Use `work` profile and project name to create a folder with multiple files inside it

```sh
template work "fix-bug-123"
```

### Use `log` profile to create a `.log` from `stderr` and `stdout` from a `curl` command

```sh
curl -X GET "https://httpbin.org/delay/3" -H  "accept: application/json" 2>&1 | template log
```

### Specify file pattern and project name

```sh
template -file "{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}-{{.ProjectName}}.txt" test
```

### Specify file pattern, project name, and piped input

```sh
history | template -file "{{.Year}}-{{.Month}}-{{.Day}}-{{.Hour}}-{{.Minute}}-{{.Second}}-{{.ProjectName}}.txt" "Hi all!" 
```

### Specify folder pattern and project name

```sh
template -dir "{{.Year}}-{{.Month}}-{{.Day}}-{{.ProjectName}}" test123
```

## Requirements

The application is a single executable, there are no special requirements.

Building application requires [go](https://en.wikipedia.org/wiki/Go_(programming_language)). You can download the installer [here](https://go.dev/doc/install).

The build is managed by [make](https://www.gnu.org/software/make/).

The default profiles use [Visual Studio Code](https://code.visualstudio.com/) to open the generated files or folders.

## Installation with Homebrew

```sh
brew tap PavelSafronov/template https://gitlab.com/PavelSafronov/template.git
brew install template
template reset
```

### Uninstall with Homebrew

```sh
brew uninstall template
brew untap PavelSafronov/template
```

## Installation with Go

If your Go installation is [properly configured](https://www.geeksforgeeks.org/how-to-install-a-package-with-go-get/), you can install `template` as a go module:

```sh
go install gitlab.com/PavelSafronov/template@latest
```

### Uninstall with go

```sh
go clean -i gitlab.com/PavelSafronov/template
```

## Manual installation

1. Navigate to [Releases](https://gitlab.com/PavelSafronov/template/-/releases)
2. Choose the latest release
3. Download the archive file `template_VERSION_OS.tar.gz`
4. From the archive file, extract `template` to a location that's in your PATH

## Installation with source

Clone the repo and run these commands from the repo directory:

```sh
make install
template reset
```

### Uninstall with source

To uninstall, run this command from the repo directory:

```sh
make uninstall
```

## Running after installation

- `template` - uses default profile "note" to create a date-stamped MD file in current folder
- `template blog` OR `template --profile blog` OR `template -p blog` - creates a date-stamped MD file in ~/Blog, opens in VSCode
- `template daily topic` OR `template --profile daily --name topic` OR `template -p daily -n topic` - creates a templated date-stamped MD file with `topic` in its name in ~/DailyNotes, opens in VSCode
- `template list` - lists all the found profiles
- `template get profile2` - lists detailed information about profile `profile2`
- `template reset` - resets the configuration to the default config
  - *Note*: this will overwrite whatever config changes you may have made
  - *Note*: the defaults may change from version to version

## Creating symbolic links

template currently refers to paths under `~`. This is to ensure that it works on all systems.

But I personally keep files in Dropbox, so I usually create symbolic links from (for example) `~/Blog` -> `~/Dropbox/Blog`. Here are the shell commands that I use, maybe they will be helpful for you. :)

PowerShell commands to create symbolic links:

```powershell
New-Item -ItemType Junction -Target (Resolve-Path "~\Dropbox\Blog") -Path "~\Blog\"
New-Item -ItemType Junction -Target (Resolve-Path "~\Dropbox\DailyNotes") -Path "~\DailyNotes\"
New-Item -ItemType Junction -Target (Resolve-Path "~\Dropbox\Notes") -Path "~\Notes\"
New-Item -ItemType Junction -Target (Resolve-Path "~\Dropbox\Therapy") -Path "~\Therapy\"
New-Item -ItemType Junction -Target (Resolve-Path "~\Dropbox\Work") -Path "~\Work\"
```

Bash commands to create symbolic links:

```bash
ln -s ~/Dropbox/Blog ~/Blog
ln -s ~/Dropbox/DailyNotes ~/DailyNotes
ln -s ~/Dropbox/Notes ~/Notes
ln -s ~/Dropbox/Therapy ~/Therapy
ln -s ~/Dropbox/Work ~/Work
```

## Dev loop

A simple way to get started is to load this directory in VSCode and debug the project using one of the prepared launch tasks. Use command `View: Show Run and Debug` to display the launch configs, then pick one and press "Start Debugging" button.

The following command will build the package, test it, and then install it:

```sh
make install
```

I use this approach, but that's because I use `template` multiple times a day, and find it helpful be using the latest version.

## Support

For anything, open an issue in this repo.

## Road map

I'm going to use this tool daily to figure out how it can be improved.

While I am the only user, this app will stay at version `0.x.y`.
At this point, breaking changes may happen.

I'll try to use feature branches, but main will sometimes be in a broken state.
Stable versions are tagged after I've had time to test it, so picking up those should be safe.

Once there are enough other users and enough new feature requirements are added, we'll transition to version `1.x.y`.

### Basic features

Here are the basic features of the CLI that I want to keep:

1. Cross-platform support
2. CLI is implemented in Go
3. Fast (under 1s) basic operation of templating file and folder names
   1. Postscripts can take longer, as long as the "basic operations" finish quickly enough.

### KISS

"Keep It Simple, Stupid"

I want to keep this CLI simple. To that extent, I'm not interested in adding too many features. My main requirement right now is: `template blog topic` should do the same thing as `template --profile blog --name topic`.

### Makefile and VSCode

Good guidance is invaluable, IMO, so to this extent I am very bullish about adding and using descriptive Makefile and VS Code launch tasks.

Makefile should be used for any build/test/release task. If there is a reason to run a task more than once *in the lifetime of a project*, add the task to the Makefile with a descriptive name.
I don't want to see complicated steps embedded in READMEs or (worse) in code.
If the task is important, one that a maintainer/developer/user *may* want to do multiple times, add it to Makefile.

Similarly, if there is a good VSCode debug scenario, add a task to launch.json.

### Future Features

Here is the future feature list:

1. Publish Chocolatey version of app, possibly using [this GoReleaser Chocolatey documentation](https://goreleaser.com/customization/chocolatey/)
2. Support for passing in a list of template values for custom values in the template. Use might look like this: `template work form123 --id=456 --corpOffice=SEA`. The passed-in extra values will be placed somewhere inside the template. Some value keys will not be allowed, like "time".
3. Add `template gen` to create templates from existing files/folders
4. Add `template check` to verify that a given file matches a specific template

### Go module

This project is mainly a CLI app written in Go, but it's also a Go module, so it can be used as a dependency.
I'm not entirely sure why anyone would want to use this module instead of the CLI, but it's there.
Please keep in mind that since my focus is on the CLI, CLI bugs and features will always be prioritized higher than module bugs and features.

## Contributing

Contributions are welcome, but unsolicited PRs are not.
If you want to add a particular feature, open an issue first and suggest it.
We'll discuss the feature and if I agree that it should be added, you can open a PR.

I don't want to waste my time or your time.

## Authors and acknowledgment

Your name will be listed here if you contribute.

## License

MIT license.

## Project status

This was a hackathon project to learn Go.
I'm going to be using this tool and upgrading it as I see fit.
